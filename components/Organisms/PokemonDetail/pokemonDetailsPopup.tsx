import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import urlsEndpoint from "../../../api/urlsEndpoint";
import textTitle from "../../../helper/textTitle";
import { Pokemon } from "../../../types/pokemon";
import ButtonForm from "../../Atom/Button";
import LoadingForm from "../../Atom/Loader";


interface PokemonDetailsPopupProps {
  pokemon: Pokemon;
  onClose: () => void;
  isLoading: Boolean;
}

const PokemonDetailsPopup: React.FC<PokemonDetailsPopupProps> = ({
  pokemon,
  onClose,
  isLoading,
}) => {
  const [pokemonDetails, setPokemonDetails] = useState<any>(null);
  const [description, setDescription] = useState<any>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(pokemon?.url.toString());
        // Fetch details of the Pokemon
        const result = await axios.get(
          `${urlsEndpoint.GET_DETAIL}/${pokemon?.name}`
        );

        // Get the flavor text entry in English
        const flavorText = result.data.flavor_text_entries.find(
          (entry: any) => entry.language.name === "en"
        );

        const description = flavorText
          ? flavorText.flavor_text
          : "Description not available";
        
        setDescription(description);
        setPokemonDetails(response.data);
      } catch (error) {
        console.error(`Error fetching details for :`, error);
      }
    };

    fetchData();
  }, [pokemon]);

  if (!isLoading) {
    return (
      <Dialog maxWidth="md" fullWidth open={true} onClose={onClose}>
        <LoadingForm />
      </Dialog>
    );
  }
  return (
    <div>
      <Dialog maxWidth="md" fullWidth open={true} onClose={onClose}>
        {pokemonDetails && (
          <>
            <DialogTitle>{textTitle(pokemonDetails?.name)}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                <TableContainer>
                  <Table>
                    <TableHead>
                      <TableRow style={{ background: "#f0f0f0" }}>
                        <TableCell width={200}>Description</TableCell>
                        <TableCell>Height</TableCell>
                        <TableCell>Weight</TableCell>
                        <TableCell> Base Experience</TableCell>
                        <TableCell>Stats</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow key={pokemon.id}>
                        <TableCell>
                          <Typography style={{ textAlign: "left" }}>
                            {description}
                          </Typography>
                        </TableCell>
                        <TableCell>{pokemonDetails.height}</TableCell>
                        <TableCell>{pokemonDetails.weight}</TableCell>
                        <TableCell>{pokemonDetails.base_experience}</TableCell>
                        <TableCell>
                          {/* <ul> */}
                          {pokemonDetails.stats.map((stat: any) => (
                            <li key={stat.stat.name}>
                              {stat.stat.name}: {stat.base_stat}
                            </li>
                          ))}
                          {/* </ul> */}
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </DialogContentText>
            </DialogContent>
            <DialogActions
              style={{
                marginTop: "-20px",
                marginBottom: "15px",
                marginRight: "20px",
              }}
            >
              <ButtonForm onClick={onClose}>Close</ButtonForm>
            </DialogActions>
          </>
        )}
      </Dialog>
    </div>
  );
};

export default PokemonDetailsPopup;
