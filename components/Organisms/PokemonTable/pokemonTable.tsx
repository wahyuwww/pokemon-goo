import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
} from "@mui/material";
import React, { useState } from "react";
import textTitle from "../../../helper/textTitle";
import { Pokemon } from "../../../types/pokemon";
import ButtonForm from "../../Atom/Button";
import LoadingForm from "../../Atom/Loader";

interface PokemonTableProps {
  data: Pokemon[];
  loading: Boolean;
  onDetailClick: (pokemon: Pokemon) => void;
}

const PokemonTable: React.FC<PokemonTableProps> = ({
  data,
  onDetailClick,
  loading,
}) => {
  const [sortBy, setSortBy] = useState<string>("");
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");

  const handleSort = (property: string) => {
    const newOrder =
      sortBy === property && sortOrder === "asc" ? "desc" : "asc";
    setSortBy(property);
    setSortOrder(newOrder);
  };

  const sortedData = [...data]?.sort((a, b) => {
    const order = sortOrder === "asc" ? 1 : -1;
    return a.name.localeCompare(b.name) * order;
  });

  if (!loading) {
    return <LoadingForm />;
  }

  return (
    <div>
      <Paper elevation={3}>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow style={{ background: "#eee" }}>
                <TableCell>
                  <TableSortLabel
                    active={sortBy === "id"}
                    direction={sortBy === "id" ? sortOrder : "asc"}
                    onClick={() => handleSort("id")}
                  >
                    No
                  </TableSortLabel>
                </TableCell>
                <TableCell>
                  <TableSortLabel
                    active={sortBy === "name"}
                    direction={sortBy === "name" ? sortOrder : "asc"}
                    onClick={() => handleSort("name")}
                  >
                    Name
                  </TableSortLabel>
                </TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Details</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {sortedData?.map((pokemon, index) => (
                <TableRow
                  key={pokemon.id}
                  style={{ background: index % 2 === 0 ? "#f9f9f9" : "white" }}
                >
                  <TableCell>{index + 1}</TableCell>
                  <TableCell>{textTitle(pokemon?.name)}</TableCell>
                  <TableCell>{textTitle(pokemon?.types?.join(", "))}</TableCell>
                  <TableCell>
                    <ButtonForm onClick={() => onDetailClick(pokemon)}>
                      Detail
                    </ButtonForm>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
};

export default PokemonTable;
