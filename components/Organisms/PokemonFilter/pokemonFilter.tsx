import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React from "react";
import textTitle from "../../../helper/textTitle";


interface PokemonFilterProps {
  types: string[];
  onFilterChange: (type: string) => void;
}

const PokemonFilter: React.FC<PokemonFilterProps> = ({
  types,
  onFilterChange,
}) => {
  const handleFilterChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    onFilterChange(event?.target?.value as string);
  };

  return (
    <div>
      <FormControl style={{ marginBottom: 20, width: "150px" }}>
        <InputLabel>Type</InputLabel>
        <Select onChange={handleFilterChange}>
          <MenuItem value="">All</MenuItem>
          {types?.map((type) => (
            <MenuItem key={type} value={type}>
              {textTitle(type)}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default PokemonFilter;
