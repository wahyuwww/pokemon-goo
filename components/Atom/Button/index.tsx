import Button from "@mui/material/Button";
import React from "react";
import styles from "./button.module.css";

interface ButtonProps {
  onClick: () => void;
  children: React.ReactNode;
}

const ButtonForm: React.FC<ButtonProps> = ({
  onClick,
  children,
}) => {
  return (
    <Button  className={styles.customButton} onClick={onClick}>
      {children}
    </Button>
  );
};

export default ButtonForm;
