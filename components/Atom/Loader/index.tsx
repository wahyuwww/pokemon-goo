import { CircularProgress } from "@mui/material";
import React from "react";
import styles from "./loader.module.css";

interface LoadingProps {
}

const LoadingForm: React.FC<LoadingProps> = () => {
  return (
    <div className={styles.isLoading}>
      <CircularProgress color="inherit" size={50} />
    </div>
  );
};

export default LoadingForm;
