import AppBar from "@mui/material/AppBar";
import Typography from "@mui/material/Typography";
import React from "react";
import styles from "./footer.module.css";

const Footer: React.FC = () => {
  return (
    <AppBar className={styles.footer} position="static" color="primary">
      <div className={styles.footerText}>
        <Typography variant="body2" color="inherit">
          © 2024 Pokemon Go. All rights reserved.
        </Typography>
      </div>
    </AppBar>
  );
};

export default Footer;
