const urlsEndpoint = {
  GET_ALL: `https://pokeapi.co/api/v2/pokemon?limit=150`,
  GET_TYPES: "https://pokeapi.co/api/v2/type",
  GET_DETAIL: `https://pokeapi.co/api/v2/pokemon-species`,
};

export default urlsEndpoint;
