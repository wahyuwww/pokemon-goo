import { Container } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import PokemonDetailsPopup from "../../components/Organisms/PokemonDetail/pokemonDetailsPopup";
import PokemonFilter from "../../components/Organisms/PokemonFilter/pokemonFilter";
import PokemonTable from "../../components/Organisms/PokemonTable/pokemonTable";
import styles from "../../styles/index.module.css";
import { Pokemon } from "../../types/pokemon";
import urlsEndpoint from "../api/urlsEndpoint";

const PokemonPage: React.FC = () => {
  // State
  const [pokemonData, setPokemonData] = useState<Pokemon[]>([]);
  const [filteredData, setFilteredData] = useState<Pokemon[]>([]);
  const [types, setTypes] = useState<string[]>([]);
  const [selectedType, setSelectedType] = useState<string>("");
  const [selectedPokemon, setSelectedPokemon] = useState<Pokemon | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      setIsLoading(false);
      // get data the first time
      const response = await axios.get(urlsEndpoint.GET_ALL);
      const pokemonList = response.data.results;

      // Fetch types for each Pokemon
      const typesPromises = pokemonList.map(
        async (pokemon: any, index: number) => {
          const pokemonDetailsResponse = await axios.get(pokemon.url);
          const types = pokemonDetailsResponse.data.types.map(
            (type: any) => type.type.name
          );
          return {
            id: index + 1,
            url: pokemon.url,
            name: pokemon.name,
            types: types,
          };
        }
      );

      const typesData = await Promise.all(typesPromises);
      setIsLoading(true);
      setPokemonData(typesData);
      setFilteredData(typesData);
    } catch (error) {
      setIsLoading(false);
      console.error("Error fetching Pokemon data:", error);
    }
  };

  useEffect(() => {
    // displays filter dropdown data
    const allTypes = pokemonData.reduce<string[]>((acc, pokemon) => {
      pokemon.types?.forEach((type) => {
        if (!acc.includes(type)) {
          acc.push(type);
        }
      });
      return acc;
    }, []);

    setTypes(allTypes);
  }, [pokemonData]);

  useEffect(() => {
    // filter by type
    const fetchPokemonByType = async () => {
      if (selectedType) {
        try {
          const response = await axios.get(
            `${urlsEndpoint.GET_TYPES}/${selectedType}`
          );
          setIsLoading(false);
          const pokemonOfType = response?.data.pokemon
            .slice(0, 10)
            .map((pokemon: any) => ({
              id: pokemon?.pokemon.url.split("/").reverse()[1],
              name: pokemon?.pokemon.name,
              types: [selectedType],
              url: pokemon?.pokemon.url,
            }));
          setIsLoading(false);

          const filteredPokemon = pokemonOfType.filter(
            (pokemon: any) => pokemon.name.length > 5
          );
          setIsLoading(true);
          setFilteredData(filteredPokemon);
        } catch (error) {
          console.error(
            `Error fetching Pokemon of type ${selectedType}:`,
            error
          );
        }
      } else {
        setFilteredData(pokemonData);
      }
    };

    fetchPokemonByType();
  }, [selectedType, pokemonData]);

  // Handle Action
  const handleFilterChange = (type: string) => {
    setSelectedType(type);
  };

  const handleDetailClick = (pokemon: Pokemon) => {
    setSelectedPokemon(pokemon);
  };

  const handleClosePopup = () => {
    setSelectedPokemon(null);
  };

  return (
    <div className={styles.distance}>
      <Container>
        <PokemonFilter types={types} onFilterChange={handleFilterChange} />
        <PokemonTable
          loading={isLoading}
          data={filteredData}
          onDetailClick={handleDetailClick}
        />

        {selectedPokemon && (
          <PokemonDetailsPopup
            isLoading={isLoading}
            pokemon={selectedPokemon}
            onClose={handleClosePopup}
          />
        )}
      </Container>
    </div>
  );
};

export default PokemonPage;
