import React from "react";
import Footer from "../components/Molecules/Footer";
import Header from "../components/Molecules/Header";
import PokemonPage from "./app/index";


const HomePage: React.FC = () => {
  return (
    <div>
      <Header></Header>
      <PokemonPage />
      <Footer></Footer>
    </div>
  );
};

export default HomePage;
